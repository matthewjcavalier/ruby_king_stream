require "./lib/ruby_king/room.rb"
require "test/unit"

class TestRoom< Test::Unit::TestCase

  def test_room_to_points
    test_room = Room.new(Coordinate.new(0,0), 2, 2)
    test_room_point_set = RoomUtil.room_to_string_points(test_room)

    for row in 0..2 do
      for col in 0..2 do
        assert(test_room_point_set.include? "#{row}:#{col}")
      end
    end
  end

  def test_room_overlaps_true_straight_overlap
    room1 = Room.new(Coordinate.new(0,0), 3, 3)
    room2 = Room.new(Coordinate.new(3,3), 3, 3)

    assert(room1.overlaps? room2)
  end

  def test_room_overlaps_true_touchy
    room1 = Room.new(Coordinate.new(0,0), 3, 3)
    room2 = Room.new(Coordinate.new(3,4), 3, 3)

    assert(room1.overlaps? room2)
  end

  def test_room_overlaps_true_buffered
    room1 = Room.new(Coordinate.new(0,0), 3, 3)
    room2 = Room.new(Coordinate.new(5,5), 3, 3)

    assert(room1.overlaps? room2)
  end

  def test_room_overlaps_true_plus_sign
    room1 = Room.new(Coordinate.new(1,3), 4, 2)
    room2 = Room.new(Coordinate.new(2,1), 2, 7)

    assert(room1.overlaps? room2)
  end

  def test_room_overlaps_room_in_room
    room1 = Room.new(Coordinate.new(0,0), 10, 10)
    room2 = Room.new(Coordinate.new(4,4), 2, 2)

    assert(room1.overlaps? room2)
  end

end


require_relative "./ruby_king/generator.rb"
require_relative "./ruby_king/display.rb"

settings = {
  "rows" => 21,
  "cols" => 80,
  "min_rooms" => 5,
  "buffer_rooms" => 5,
  "min_room_height" => 3,
  "min_room_width" => 2,
  "room_height_buffer" => 5,
  "room_width_buffer" => 5,
  "room_char" => ".",
  "hall_char" => "#",
  "wall_char" => " ",
  "border_char" => "+",
  "max_hardness" => 50,
}

srand(1234)

dungeon_floor = Generator.gen_floor(settings)

Display.draw_floor(dungeon_floor, settings)
Display.draw_floor_hardness(dungeon_floor)

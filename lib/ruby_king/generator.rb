require_relative "tile.rb"
require_relative "room.rb"

# Mega Ultra Delux generation module for all of your generation needs
module Generator
  def self.gen_floor(settings)
    floor = gen_floor_frame(settings)
    apply_borders(floor, settings)
    rooms = gen_rooms(settings)

    with_rooms = apply_rooms(floor, rooms)

    with_rooms
  end

  def self.gen_floor_frame(settings)
    rows = settings["rows"]
    cols = settings["cols"]
    max_hardness = settings["max_hardness"]

    Array.new(rows) { Array.new(cols) { Tile.new(TileType::WALL, rand(max_hardness)) } }
  end

  def self.apply_borders(floor, settings)
    floor[0] = _build_border_array(settings["cols"], settings["max_hardness"])
    floor[floor.length - 1] = _build_border_array(settings["cols"], settings["max_hardness"])
    floor.each do |row|
      row[0].type = TileType::BORDER
      row[row.length - 1].type = TileType::BORDER
    end
    floor
  end

  def self._build_border_array(cols, hardness)
    Array.new(cols) {
      Tile.new(TileType::BORDER, hardness)
    }
  end

  def self.gen_rooms(settings)
    rooms = []

    while rooms.length < settings["min_rooms"] + rand(settings["buffer_rooms"])
      new_room = RoomUtil.gen_rand_room(settings)

      rooms_have_overlap = false

      rooms.each do |room|
        rooms_have_overlap |= room.overlaps? new_room
      end

      rooms << new_room unless rooms_have_overlap
    end
    rooms.each do |room|
      puts room.to_s
    end

    rooms
  end

  def self.apply_rooms(floor, rooms)
    rooms.each do |room|
      RoomUtil.room_to_points(room).each do |coord|
        floor[coord.row][coord.col].type = TileType::ROOM
      end
    end
    floor
  end
end

require_relative "tile.rb"

# Handling all of the display functionality
module Display
  def self.draw_floor(floor, settings)
    display_str = ""

    floor.each do |row|
      row.each do |col|
        display_str.concat(get_tile_symbol(col.type, settings))
      end
      display_str.concat("\n")
    end
    puts display_str
  end

  def self.get_tile_symbol(type, settings)
    case type
    when TileType::WALL
      settings["wall_char"]
    when TileType::BORDER
      settings["border_char"]
    when TileType::HALL
      settings["hall_char"]
    when TileType::ROOM
      settings["room_char"]
    else
      "?"
    end
  end

  def self.draw_floor_hardness(floor)
    display_str = ""

    floor.each do |row|
      row.each do |col|
        display_str.concat((col.hardness % 10).to_s)
      end
      display_str.concat("\n")
    end
    puts display_str
  end
end

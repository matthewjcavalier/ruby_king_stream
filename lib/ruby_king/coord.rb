# A simple row:col coordinate
class Coordinate
  attr_accessor :row
  attr_accessor :col

  def initialize(row, col)
    @row = row
    @col = col
  end

  def to_s
    "row = #{@row}, col = #{@col}"
  end
end

# All classes and modules pertaining to Tiles

module TileType
  WALL = 1
  BORDER = 2
  HALL = 3
  ROOM = 4
end

# Holds dugeon floor tile state reprsentation
class Tile
  attr_accessor :hardness
  attr_reader :type

  def initialize(type = TileType::WALL, hardness = 1)
    @type = type
    @hardness = hardness
  end

  def type=(new_type)
    @type = new_type
    case new_type
    when TileType::BORDER
      @hardness = 1_000_000
    when TileType::ROOM
      @hardness = 0
    when TileType::HALL
      @hardness = 0
    end
  end
end

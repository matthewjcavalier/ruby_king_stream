require_relative "coord.rb"
require "set"

# simplistic representaiton of a room
class Room
  attr_accessor :top_left
  attr_accessor :height
  attr_accessor :width

  def initialize(top_left, height, width)
    @top_left = top_left
    @height = height
    @width = width
  end

  def overlaps?(room)
    this_room_points = RoomUtil.room_to_string_points(get_bigger_room(self))
    other_room_points = RoomUtil.room_to_string_points(get_bigger_room(room))

    this_room_points.intersect? other_room_points
  end

  def get_bigger_room(room)
    Room.new(Coordinate.new(room.top_left.row - 1, room.top_left.col - 1),
             room.height + 2,
             room.width + 2)
  end

  def to_s()
    "top_left = {#{@top_left.to_s}}, height = #{@height}, width = #{@width}"
  end
end

# Helper functions for working on Rooms
module RoomUtil
  def self.gen_rand_room(settings)
    occupied_places = 2
    height = settings["min_room_height"] + rand(settings["room_height_buffer"])
    width = settings["min_room_width"] + rand(settings["room_width_buffer"])
    col = rand(settings["cols"] - occupied_places - width) + 1
    row = rand(settings["rows"] - occupied_places - height) + 1

    Room.new(Coordinate.new(row, col), height, width)
  end

  def self.room_to_points(room)
    room_set = Set.new
    for row in room.top_left.row..room.top_left.row + room.height
      for col in room.top_left.col..room.top_left.col + room.width
        room_set.add(Coordinate.new(row, col))
      end
    end
    room_set
  end

  def self.room_to_string_points(room)
    new_set = Set.new

    room_to_points(room).each do |point|
      new_set.add(point.row.to_s + ":" + point.col.to_s)
    end

    new_set
  end
end
